
ABOUT TEAMENU
--------------

TeaMenu is a Drupal 8 module for providing expandable, responsive, accessible, keyable menus.

INSTALLATION
------------

(1) Copy the 2 twig templates from the teamenu/templates directory to your theme's /templates directory.

Note: if you copy the templates to a core or contributed theme, they may be overwritten by upgrades.

If this happens, simply re-copy the templates to the theme(s) after the upgrade.

(2) Enable the module.

CONFIGURATION
-------------

The module adds a "TeaMenu" checkbox to each Menu Block.

Tick the "TeaMenu" checkbox to activate the TeaMenu functionality for a Menu Block.
